package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature {

	public Fahrenheit( float t ) {
		super(t);
	}
	
	public String toString() {
		return Float.toString( this.value );
	}

	@Override
	public Temperature toCelsius() {
		this.value = ((( this.value - 32)*5)/9);
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		return this;
	}

}
