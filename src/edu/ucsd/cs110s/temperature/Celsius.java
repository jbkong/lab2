package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {

	public Celsius( float t ) {
		super(t);
	}
	
	public String toString() {
		return Float.toString( this.value );
	}

	@Override
	public Temperature toCelsius() {
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		this.value = ( this.value * 9 / 5 ) + 32;
		return this;
	}
	
}
